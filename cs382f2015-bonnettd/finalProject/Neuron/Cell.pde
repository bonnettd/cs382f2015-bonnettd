public class Cell {
  int x, y, z;
  Ellipsoid ball;    //declaring an ellipsoid
  BezTube t1, t2, t3, t4, t5, t6;    //declraring 6 different bezTubes
  
  //Toroid variables
  Toroid toro1, toro2;
  Rot roti, roti2;           //declaring a new Rot (Rotation) object
  float[] tAngle, tAngle2;   //float array for stroing the different angles the toroid will be drawn at
  float spd = 0.002f;       //the speed the toroid is moving
  float spd2 = 0.002f;       //the speed the toroid is moving
  float l = 1;               //the toroid position along whatever tube it is on
  float n = 0;               //the toroid position along whatever tube it is on
  float dl = spd;            //temp variable
  float dl2 = spd2;          //temp variable
  
  //Cell constructor
  Cell(PApplet papp, int xpos, int ypos, int zpos) {
    x = xpos;
    y = ypos;
    z = zpos;
    
    //Creating the ellipsoid that will represent the nucleus
   ball = new Ellipsoid(papp, 20, 30);
   ball.setRadius(40, 30, 40);
   ball.setTexture("blur.png");
   ball.drawMode(Shape3D.TEXTURE);
  
  
  //information for the first toroid that starts at the nucleus and travles along t1 
  toro1 = new Toroid(papp, 20, 20);
  toro1.fill(color(250, 120, 20));
 // toro1.setTexture("gray2.jpg");
  toro1.setRadius(10, 30, 8);
  toro1.drawMode(Shape3D.SOLID);
  
  //information for the second toroid that starts at the bottom of t2 and travels along t2 until it reaches the nucleus
  toro2 = new Toroid(papp, 20, 20);
  toro2.fill(color(250, 120, 20));
 // toro2.setTexture("gray2.jpg");
  toro2.setRadius(10, 30, 8);
  toro2.drawMode(Shape3D.SOLID);
  
   
   //Vectors used for the tubes coming out of the nucleus
   //The vectors are the paths that the tubes will follow
    PVector[] top = new PVector[] {
    new PVector(x, y-30, 0),
    new PVector(x+175, -500, 0),
    new PVector(x+200, -1000, 0),
    new PVector(x, -3000, 0),
  };
  
    PVector[] bottom = new PVector[] {
    new PVector(x, y+30, 0),
    new PVector(x-160, 400, 0),
    new PVector(x-200, 1000, 0),
    new PVector(x, 3000, 0),
  };
  
    PVector[] right = new PVector[] {
    new PVector(x+40, y, 0),
    new PVector(x+400, y-200, 0),
    new PVector(3000, y, 0),
  };
  
    PVector[] left = new PVector[] {
    new PVector(x-40, y, 0),
    //new PVector(x-300, y-300, 0),
    new PVector(x-1000, y-150, 0),
    new PVector(-3000, y, 0),
  };
  
    PVector[] p1 = new PVector[] {
    new PVector(x+8, y-2, z+38),
    new PVector(x+8, y-200, 3000),
  };
  
  PVector[] p2 = new PVector[] {
    new PVector(x-8, y-2, z-38),
    new PVector(x-8, y-200, -3000),
  };
  
  
  //Creating all the different tubes (t1-t6)
  t1 = new BezTube(papp, new P_Bezier3D(top, top.length), 6, 10, 8);
  t1.fill(color(3, 136, 250));
  t1.drawMode(Shape3D.SOLID);
  
  t2 = new BezTube(papp, new P_Bezier3D(bottom, bottom.length), 6, 10, 8);
  t2.fill(color(3, 136, 250));
  t2.drawMode(Shape3D.SOLID);
  
  t3 = new BezTube(papp, new P_Bezier3D(right, right.length), 6, 10, 8);
  t3.fill(color(3, 136, 250));
  t3.drawMode(Shape3D.SOLID);
  
  t4 = new BezTube(papp, new P_Bezier3D(left, left.length), 6, 10, 8);
  t4.fill(color(3, 136, 250));
  t4.drawMode(Shape3D.SOLID);
  
  t5 = new BezTube(papp, new P_Bezier3D(p1, p1.length), 6, 10, 8);
  t5.fill(color(3, 136, 250));
  t5.drawMode(Shape3D.SOLID);
  
  t6 = new BezTube(papp, new P_Bezier3D(p2, p2.length), 6, 10, 8);
  t6.fill(color(3, 136, 250));
  t6.drawMode(Shape3D.SOLID);
     
  } //constructor
  
  void drawCell() {
    //drawing the ellipsoid to represent the nucleus
    pushMatrix();
    translate(x, y, z);
    ball.draw();
    popMatrix();
    
    //drawing all of the tubes
    pushMatrix();
    translate(0, 10, 0);
    t1.draw();
    popMatrix();
    
    pushMatrix();
    translate(0, -5, 0);
    t2.draw();
    popMatrix();
    
    pushMatrix();
    translate(-5, 0, 0);
    t3.draw();
    popMatrix();
    
    pushMatrix();
    translate(5, 0, 0);
    t4.draw();
    popMatrix();
    
    pushMatrix();
    translate(-5, 0, -22);
    rotateX(radians(20));
    t5.draw();
    popMatrix();
    
    pushMatrix();
    translate(4, 0, 23);
    rotateX(radians(-20));
    t6.draw();
    popMatrix();
    
    pushMatrix();
    translate(4, 0, 20);
    rotateX(radians(20));
    t6.draw();
    popMatrix();
    
    pushMatrix();
    translate(-8, 0, -10);
    rotateX(radians(-20));
    t5.draw();
    popMatrix();
    
    //***********************************************************************
    //The following is code for drawing the toroids and adding their animaitons
    //***********************************************************************
    l += dl2;    // setting l to have an initial value of 1 + d2
    n -= dl;    // setting n to have an initial value of 0 - dl;
    
   if (n <= 0) {
   dl = -spd;
 } //if
  else if (n >= 1) {
   toro1.moveTo(t1.getPoint(0));
   n -= 1;
 } //else if
    
    
   if (l >= 1) {
   dl2 = -spd;
 } //if
 else if (l <= 0) {
   dl2 = 0;
   l = 0;
   pushMatrix();
   toro2.moveTo(t2.getPoint(1));
   popMatrix();
   l+=1;
 } //else if
 
    
  roti = new Rot(new PVector(0, 1, 0), t1.getTangent(n));
  tAngle = roti.getAngles(RotOrder.XYZ);
  toro1.moveTo(t1.getPoint(n));
  toro1.rotateTo(tAngle);
  toro1.draw();
  
  roti2 = new Rot(new PVector(0, 1, 0), t2.getTangent(l));
  tAngle2 = roti2.getAngles(RotOrder.XYZ);
  toro2.moveTo(t2.getPoint(l));
  toro2.rotateTo(tAngle2);
  toro2.draw();
    
    
  } //drawCell
  
  
  
} //cell class